---
title: "Announcing March 20, 2017 Critical Security Update"
date: 2017-03-17
author: Brian Neel
author_twitter: b0bby_tables
categories: releases
tags: security, releases
---

On Monday, March 20th, 2017 at 23:59 UTC, we will publish a critical GitLab
security update. More details will be forthcoming on [our blog], including
which versions of GitLab are affected.

We recommend installations running affected versions to upgrade
immediately. Please forward this alert to the appropriate people at your
organization and have them subscribe to [Security Notices].

[our blog]: /blog
[Security Notices]: /company/contact/#security-notices

<!-- more -->
