---
layout: markdown_page
title: "Ops products team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Engineers in this team are embedded in the **Ops** cross-functional group and
are responsible for coverage of **Ops** stage
[product categories](/handbook/product/categories/).

## Areas of Responsibility

* **[Verify]**: CI tools
  * Third-party CI: Jenkins, Bamboo, etc.
* **[Package]**: Registry Integration for containers, binaries
* **[Release]**: Continuous Delivery, Review Apps, Release Automation, Feature Flags, Pages
* **[Configure]**: Application, Infrastructure, Auto DevOps
* **[Monitor]**: APM, Logging, Tracking
* **[Secure]**: Security Products

## Members

TBD

[Verify]: /handbook/product/categories/#Verify
[Package]: /handbook/product/categories/#Package
[Release]: /handbook/product/categories/#Release
[Configure]: /handbook/product/categories/#Configure
[Monitor]: /handbook/product/categories/#Monitor
[Secure]: /handbook/product/categories/#Secure
